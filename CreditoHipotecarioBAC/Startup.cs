﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CreditoHipotecarioBAC.Startup))]
namespace CreditoHipotecarioBAC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
