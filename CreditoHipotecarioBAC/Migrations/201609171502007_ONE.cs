namespace CreditoHipotecarioBAC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ONE : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.personjuridic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameJuridic = c.String(nullable: false, maxLength: 50),
                        NameComercial = c.String(nullable: false, maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.person", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.NameJuridic, unique: true, name: "INDEX_PERSON_NAME_JURIDIC")
                .Index(t => t.NameComercial, unique: true, name: "INDEX_PERSON_NAME_COMERCIAL")
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.person",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TypeIdentificationId = c.Int(nullable: false),
                        Identification = c.String(nullable: false, maxLength: 20),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.typeidentification", t => t.TypeIdentificationId, cascadeDelete: true)
                .Index(t => t.Name, unique: true, name: "INDEX_ADM_PERSONA")
                .Index(t => t.TypeIdentificationId)
                .Index(t => t.Identification, unique: true, name: "INDEX_ADM_PERSONA_IDENTIFICACION");
            
            CreateTable(
                "dbo.typeidentification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                        Description = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "INDEX_ADM_TYPEIDENTIFICATION");
            
            CreateTable(
                "dbo.personnatural",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 30),
                        LastName = c.String(nullable: false, maxLength: 30),
                        Active = c.Boolean(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.person", t => t.PersonId, cascadeDelete: true)
                .Index(t => new { t.FirstName, t.LastName }, unique: true, name: "INDEX_PERSON_NATURAL")
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.personnatural", "PersonId", "dbo.person");
            DropForeignKey("dbo.personjuridic", "PersonId", "dbo.person");
            DropForeignKey("dbo.person", "TypeIdentificationId", "dbo.typeidentification");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.personnatural", new[] { "PersonId" });
            DropIndex("dbo.personnatural", "INDEX_PERSON_NATURAL");
            DropIndex("dbo.typeidentification", "INDEX_ADM_TYPEIDENTIFICATION");
            DropIndex("dbo.person", "INDEX_ADM_PERSONA_IDENTIFICACION");
            DropIndex("dbo.person", new[] { "TypeIdentificationId" });
            DropIndex("dbo.person", "INDEX_ADM_PERSONA");
            DropIndex("dbo.personjuridic", new[] { "PersonId" });
            DropIndex("dbo.personjuridic", "INDEX_PERSON_NAME_COMERCIAL");
            DropIndex("dbo.personjuridic", "INDEX_PERSON_NAME_JURIDIC");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.user");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.personnatural");
            DropTable("dbo.typeidentification");
            DropTable("dbo.person");
            DropTable("dbo.personjuridic");
        }
    }
}
