﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CreditoHipotecarioBAC.Models
{
    [Table("person")]
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_ADM_PERSONA", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre de la persona.")]
        [StringLength(100, ErrorMessage = "El nombre de la persona no debe ser mayor de 100 caracteres.")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Tipo de identificacion")]
        public int TypeIdentificationId { get; set; }

        [Index("INDEX_ADM_PERSONA_IDENTIFICACION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la identificacion de la persona.")]
        [StringLength(20, ErrorMessage = "La identificacion de la persona no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Identificacion")]
        public string Identification { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public virtual TypeIdentification TypeIdentifications { get; set; }
        //public virtual PersonNatural PersonNaturals { get; set; }
        //public virtual PersonJuridic PersonJuridics { get; set; }
    }
}