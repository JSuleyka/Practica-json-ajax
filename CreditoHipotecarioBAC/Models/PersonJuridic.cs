﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CreditoHipotecarioBAC.Models
{
    [Table("personjuridic")]
    public class PersonJuridic
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_PERSON_NAME_JURIDIC", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre juridico de la persona.")]
        [StringLength(50, ErrorMessage = "El nombre juridico de la persona no debe ser mayor de 50 caracteres.")]
        [Display(Name = "Nombre juridico")]
        public string NameJuridic { get; set; }

        [Index("INDEX_PERSON_NAME_COMERCIAL", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre comercial de la persona.")]
        [StringLength(50, ErrorMessage = "El nombre comercial de la persona no debe ser mayor de 50 caracteres.")]
        [Display(Name = "Nombre comercial")]
        public string NameComercial { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public int PersonId { get; set; }

        public virtual Person Persons { get; set; }
    }
}