﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CreditoHipotecarioBAC.Models
{
    [Table("typeidentification")]
    public class TypeIdentification
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_ADM_TYPEIDENTIFICATION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el tipo de identificacion.")]
        [StringLength(30, ErrorMessage = "El tipo de identificacion no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Tipo de identificacion")]
        public string Name { get; set; }

        [StringLength(50, ErrorMessage = "La descripcion no debe ser mayor de 50 caracteres.")]
        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
}