﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CreditoHipotecarioBAC.Models
{
    [Table("personnatural")]
    public class PersonNatural
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_PERSON_NATURAL", 1, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre de la persona.")]
        [StringLength(30, ErrorMessage = "El nombre de la persona no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        [Index("INDEX_PERSON_NATURAL", 2, IsUnique = true)]

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el apellido de la persona.")]
        [StringLength(30, ErrorMessage = "El apellido de la persona no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public int PersonId { get; set; }

        public virtual Person Persons { get; set; }
    }
}